package tasks

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func TestValidPriority(t *testing.T) {
	if !IsValidPriority(High) {
		t.Errorf("IsValidPriority(%v) = true", High)
	}
	if !IsValidPriority(Medium) {
		t.Errorf("IsValidPriority(%v) = true", Medium)
	}
	if !IsValidPriority(Low) {
		t.Errorf("IsValidPriority(%v) = true", Low)
	}
	if !IsValidPriority(None) {
		t.Errorf("IsValidPriority(%v) = true", None)
	}
}
func TestNotValidPriority(t *testing.T) {
	randInteger := rand.Int() + High + 1
	if IsValidPriority(randInteger) {
		t.Errorf("IsValidPriority(%v) = true", randInteger)
	}
}

func TestValidStatus(t *testing.T) {
	if !IsValidStatus(Todo) {
		t.Errorf("IsValidStatus(%v) = true", Todo)
	}
	if !IsValidStatus(Working) {
		t.Errorf("IsValidStatus(%v) = true", Working)
	}
	if !IsValidStatus(Deleted) {
		t.Errorf("IsValidStatus(%v) = true", Deleted)
	}
	if !IsValidStatus(Done) {
		t.Errorf("IsValidStatus(%v) = true", Done)
	}
}
func TestNotValidStatus(t *testing.T) {
	testString := "test"
	if IsValidStatus(testString) {
		t.Errorf("IsValidStatus(%v) = true", testString)
	}
}

func TestValidFilter(t *testing.T) {
	if !IsValidFilter(priority) {
		t.Errorf("IsValidFilter(%v) = true", priority)
	}
	if !IsValidFilter(deadline) {
		t.Errorf("IsValidFilter(%v) = true", deadline)
	}
	if !IsValidFilter(addedTime) {
		t.Errorf("IsValidFilter(%v) = true", addedTime)
	}
}
func TestNotValidFilter(t *testing.T) {
	testString := "test"
	if IsValidFilter(testString) {
		t.Errorf("IsValidFilter(%v) = true", testString)
	}
}

func TestGetPriority(t *testing.T) {
	task := Task{}
	task.Priority = None
	if task.getPriority() != "None" {
		t.Errorf("task.Priority = None, t.getPriority() != None")
	}
	task.Priority = Low
	if task.getPriority() != "Low" {
		t.Errorf("task.Priority = Low, t.getPriority() != Low")
	}
	task.Priority = Medium
	if task.getPriority() != "Medium" {
		t.Errorf("task.Priority = Medium, t.getPriority() != Medium")
	}
	task.Priority = High
	if task.getPriority() != "High" {
		t.Errorf("task.Priority = High, t.getPriority() != High")
	}
}

func TestTaskString(t *testing.T) {
	task := Task{
		ID:          0,
		Title:       `title`,
		Description: `description`,
		Priority:    None,
		Deadline:    time.Time{},
		Estimate:    time.Duration(0),
		Spent:       time.Duration(0),
		Status:      Todo,
	}
	str := "id:\t\t\t" + fmt.Sprint(task.ID) + "\n"
	str += "title:\t\t\t" + task.Title + "\n"
	str += "description:\t\t" + task.Description + "\n"
	str += "priority:\t\t" + task.getPriority() + "\n"
	str += "deadline:\t\t" + task.Deadline.String() + "\n"
	str += "time estimate:\t\t" + task.Estimate.String() + "\n"
	str += "time spent:\t\t" + task.Spent.String() + "\n"
	str += "status:\t\t\t" + task.Status + "\n"
	str += "started working:\t" + task.StartWork.String() + "\n"
	str += "ended working:\t\t" + task.EndWork.String() + "\n"
	if task.String() != str {
		t.Error("failed at generating a proper String() of task")
	}
}
