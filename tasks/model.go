package tasks

import (
	"fmt"
	"sort"
	"strings"
	"time"
)

// bytes
type bytes []byte

// tasks constants
const (
	None = iota
	Low
	Medium
	High
)

// status constants
const (
	All     = "all"
	Todo    = "todo"
	Working = "working"
	Deleted = "deleted"
	Done    = "done"
)

// filter constants
const (
	priority  = "priority"
	deadline  = "deadline"
	addedTime = "addedtime"
)

// IsValidPriority checks if an int is a valid priority
func IsValidPriority(v int) bool {
	return (v == High || v == Medium || v == Low || v == None)
}

// IsValidStatus checks if a string is a valid status
func IsValidStatus(v string) bool {
	return (v == All ||
		v == Todo ||
		v == Working ||
		v == Deleted ||
		v == Done)
}

// IsValidFilter checks if a string is a valid filter
func IsValidFilter(v string) bool {
	str := strings.ToLower(v)
	return (str == priority ||
		str == deadline ||
		str == addedTime ||
		str == "")
}

// TaskList array of Task
type TaskList []Task

// Task -> simple task info
type Task struct {
	ID          int           `json:"id"`
	Title       string        `json:"title"`
	Description string        `json:"description"`
	Priority    int           `json:"priority"`
	Deadline    time.Time     `json:"deadline"`
	Estimate    time.Duration `json:"estimate"`
	Spent       time.Duration `json:"spent"`
	StartWork   time.Time     `json:"startWork"`
	EndWork     time.Time     `json:"endWork"`
	Status      string        `json:"status"`
}

func (t *Task) getPriority() string {
	return [...]string{"none", "low", "medium", "high"}[t.Priority]
}

func (t *Task) String() string {
	str := "id:\t\t\t" + fmt.Sprint(t.ID) + "\n"
	str += "title:\t\t\t" + t.Title + "\n"
	str += "description:\t\t" + t.Description + "\n"
	str += "priority:\t\t" + t.getPriority() + "\n"
	str += "deadline:\t\t" + t.Deadline.String() + "\n"
	str += "time estimate:\t\t" + t.Estimate.String() + "\n"
	str += "time spent:\t\t" + t.Spent.String() + "\n"
	str += "status:\t\t\t" + t.Status + "\n"
	str += "started working:\t" + t.StartWork.String() + "\n"
	str += "ended working:\t\t" + t.EndWork.String() + "\n"
	return str
}

func sortTasks(tasks []Task, filter string) {
	statVals := map[string]int{Todo: 0, Working: 1, Done: 2, Deleted: 3}
	sort.SliceStable(tasks, func(i, j int) bool {
		switch filter {
		case priority:
			return tasks[i].Priority < tasks[j].Priority
		case deadline:
			return tasks[i].Deadline.Before(tasks[j].Deadline)
		case addedTime:
			// added time = time spent on task - time estimated on task
			a := tasks[i].Spent - tasks[i].Estimate
			b := tasks[j].Spent - tasks[j].Estimate
			return a < b
		default:
			// fills the order of [todo,working,done,closed]
			return statVals[tasks[i].Status] < statVals[tasks[j].Status]
		}
	})
}
