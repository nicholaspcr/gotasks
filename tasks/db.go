package tasks

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/boltdb/bolt"
)

const boltPerm = os.FileMode(0600)

// path in which all the data is stored
var dbPath string = os.Getenv("HOME") + "/.gotasks"

// global bucket that contains all of the below buckets
var generalBucket []byte = []byte("general")
var todoBucket []byte = []byte(Todo)
var workingBucket []byte = []byte(Working)
var deteledBucket []byte = []byte(Deleted)
var doneBucket []byte = []byte(Done)

// DBsetup prepares the buckets to be used
func DBsetup() error {
	// creates directory
	checkPathDB(dbPath)
	dbPath += "/bolt.db"
	// creates buckets
	db, err := bolt.Open(dbPath, boltPerm, nil)
	defer db.Close()
	if err != nil {
		return fmt.Errorf("could not open db, %v", err)
	}
	err = db.Update(func(tx *bolt.Tx) error {
		root, err := tx.CreateBucketIfNotExists(generalBucket)
		if err != nil {
			return fmt.Errorf("could not create 'general' bucket: %v", err)
		}
		_, err = root.CreateBucketIfNotExists(todoBucket)
		if err != nil {
			return fmt.Errorf("could not create 'todo' bucket: %v", err)
		}
		_, err = root.CreateBucketIfNotExists(workingBucket)
		if err != nil {
			return fmt.Errorf("could not create 'working' bucket: %v", err)
		}
		_, err = root.CreateBucketIfNotExists(deteledBucket)
		if err != nil {
			return fmt.Errorf("could not create 'closed' bucket: %v", err)
		}
		_, err = root.CreateBucketIfNotExists(doneBucket)
		if err != nil {
			return fmt.Errorf("could not create 'done' bucket: %v", err)
		}
		return nil
	})
	if err != nil {
		return fmt.Errorf("could not set up buckets, %v", err)
	}
	return nil
}
func checkPathDB(path string) {
	var err error
	if _, err = os.Stat(path); os.IsNotExist(err) {
		// os.ModeDir
		os.Mkdir(path, os.ModePerm)
	}
	if err != nil {
		log.Fatal(err)
	}
}

// CreateTask creates a new tasks in the db
func CreateTask(t Task) {
	db, _ := bolt.Open(dbPath, boltPerm, nil)
	defer db.Close()
	db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(generalBucket)
		conflit := checkConflict(t, bucket)
		if conflit {
			return fmt.Errorf(`Can't create the tasks because other tasks don't allow enough to do estimated time`)
		}
		t.ID = getNewID(bucket)
		bucky := bucket.Bucket(todoBucket)
		v, _ := json.Marshal(t)
		k, _ := json.Marshal(t.ID)
		bucky.Put(k, v)
		return nil
	})
}

// ListTasks lists all the tasks of the db
func ListTasks(filter, status string) {
	db, _ := bolt.Open(dbPath, boltPerm, nil)
	defer db.Close()

	buckyList := getBucketListByStatus(status)
	tasks := make([]Task, 0)
	db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(generalBucket)
		taskys := getTasksFromBytesList(buckyList, bucket)
		tasks = append(tasks, taskys...)
		return nil
	})

	sortTasks(tasks, filter)
	for _, t := range tasks {
		fmt.Println(t.String())
	}
}

// MoveTask changes the status of a task
func MoveTask(id int, to string) {
	db, _ := bolt.Open(dbPath, boltPerm, nil)
	defer db.Close()
	err := db.Update(func(tx *bolt.Tx) error {
		// opening general bucket
		bucket := tx.Bucket(generalBucket)
		toBuc := bucket.Bucket([]byte(to))
		// processing key
		idBytes, _ := json.Marshal(id)
		buckyName, err := findBucketOfID(idBytes, bucket)
		if err != nil {
			return err
		}
		if string(buckyName) == to {
			return fmt.Errorf("Cannot move a task from a status to the same status")
		}
		fromBuc := bucket.Bucket(buckyName)

		t := Task{}
		taskData := fromBuc.Get(idBytes)
		err = json.Unmarshal(taskData, &t)
		fromBuc.Delete(idBytes)

		t.Status = to // manipulating Task
		// if working status sets the timer of time spent
		if to == Working {
			t.StartWork = time.Now()
		}
		// if is from working add time to spent time
		if string(buckyName) == string(workingBucket) {
			if to == Deleted || to == Done {
				t.EndWork = time.Now()
				t.Spent += t.EndWork.Sub(t.StartWork)
			} else {
				t.Spent += time.Now().Sub(t.StartWork)
				t.StartWork = time.Time{}
			}
		}
		taskData, err = json.Marshal(t)
		toBuc.Put(idBytes, taskData)
		return nil
	})
	if err != nil {
		log.Fatal(err)
	}
}

// DeleteTask moves the task to the deleted 'bucket'
func DeleteTask(id int) {
	MoveTask(id, Deleted)
}

// CloseTask mvoes the task to the closed 'bucket
func CloseTask(id int) {
	MoveTask(id, Done)
}
