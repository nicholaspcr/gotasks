package tasks

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/boltdb/bolt"
)

// bucket related functions

func getBucketListByStatus(status string) []bytes {
	var arr []bytes
	switch status {
	case All:
		arr = []bytes{todoBucket, workingBucket, doneBucket, deteledBucket}
	case Todo:
		arr = []bytes{todoBucket}
	case Working:
		arr = []bytes{workingBucket}
	case Deleted:
		arr = []bytes{deteledBucket}
	case Done:
		arr = []bytes{doneBucket}
	default:
		arr = []bytes{}
	}
	return arr
}

func getNewID(bucket *bolt.Bucket) int {
	idBytes := bucket.Get([]byte("ID"))
	var id int
	if len(idBytes) == 0 { // first id created
		id = 0
		idBytes, _ = json.Marshal(0)
	} else {
		json.Unmarshal(idBytes, &id)
	}
	newID, _ := json.Marshal(id + 1)
	bucket.Put([]byte("ID"), newID) // id for the next call
	return id
}

func getTasksFromBytesList(bytesList []bytes, bucket *bolt.Bucket) []Task {
	var tasks []Task
	for _, buckyName := range bytesList {
		bucky := bucket.Bucket(buckyName)
		taskys := getTasksInBucket(bucky)
		tasks = append(tasks, taskys...)
	}
	return tasks
}

func getTasksInBucket(bucky *bolt.Bucket) []Task {
	tasks := make([]Task, 0)
	c := bucky.Cursor()
	for k, v := c.First(); k != nil; k, v = c.Next() {
		var key int
		var t Task
		json.Unmarshal(k, &key)
		json.Unmarshal(v, &t)
		tasks = append(tasks, t)
	}
	return tasks
}

func findBucketOfID(id []byte, bucket *bolt.Bucket) ([]byte, error) {
	buckyList := []bytes{todoBucket, workingBucket}
	correctBucket := bytes{}
	for _, buckyName := range buckyList {
		bucky := bucket.Bucket(buckyName)
		info := bucky.Get(id)
		if len(info) == 0 {
			continue
		} else {
			correctBucket = buckyName
			break
		}
	}
	if len(correctBucket) == 0 {
		return nil, fmt.Errorf("Couldn't find the id in any of the buckets in the list")
	}
	return correctBucket, nil
}

func checkConflict(t Task, bucket *bolt.Bucket) bool {
	var tasks []Task
	bucky := bucket.Bucket(todoBucket)
	todoTasks := getTasksInBucket(bucky)
	bucky = bucket.Bucket(workingBucket)
	workingTasks := getTasksInBucket(bucky)
	for _, v := range todoTasks {
		if v.Deadline.Equal(time.Time{}) {
			continue
		}
		tasks = append(tasks, v)
	}
	tasks = append(tasks, workingTasks...)
	if len(tasks) == 0 {
		return false
	}

	// all valid tasks sorted by deadline
	sortTasks(tasks, deadline)

	// todo: do binary search here
	// just to get the index of the element with deadline after the task deadline
	var limit int = -1
	totalEstimate := time.Duration(0)
	for i, v := range tasks {
		if v.Deadline.After(t.Deadline) {
			limit = i
			break
		}

		timeSpent := v.Spent
		if !v.StartWork.Equal(time.Time{}) {
			timeSpent += time.Now().Sub(v.StartWork)
		}
		if timeSpent > v.Estimate {
			totalEstimate += timeSpent
		} else {
			totalEstimate += v.Estimate
		}
	}
	if limit != -1 {
		tasks = tasks[:limit]
	}

	totalTime := t.Deadline.Sub(time.Now())
	timeAvailable := totalTime - (totalEstimate + t.Estimate)
	return timeAvailable < 0
}
