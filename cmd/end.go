package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/nicholaspcr/gotasks/tasks"
)

// endCmd represents the close command
var endCmd = &cobra.Command{
	Use:   "end",
	Short: "Closes a task",
	Long: `Closes the task that contains the id given,
in doesn't delete it but moves it to a specific status 'bucket'`,
	Run: func(cmd *cobra.Command, args []string) {
		if taskID == -1 {
			log.Fatal("enter a valid taskID")
		}
		tasks.DBsetup()
		tasks.CloseTask(taskID)
	},
}

func init() {
	rootCmd.AddCommand(endCmd)
}
