package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/nicholaspcr/gotasks/tasks"
)

var moveTo string

// moveCmd represents the move command
var moveCmd = &cobra.Command{
	Use:   "move",
	Short: "change the status of the task",
	Long: `Moves the task from one status to another
Only the tasks in the 'todo' and the 'working' status can be changed`,
	Run: func(cmd *cobra.Command, args []string) {
		if taskID == -1 {
			fmt.Println("enter a valid taskID")
			os.Exit(1)
		}
		if !tasks.IsValidStatus(moveTo) {
			fmt.Println("enter a valid status/destination")
		}
		tasks.DBsetup()
		tasks.MoveTask(taskID, moveTo)
	},
}

func init() {
	rootCmd.AddCommand(moveCmd)

	// todo: change to package.working
	moveCmd.Flags().StringVarP(&moveTo,
		"to",
		"t",
		tasks.Working,
		"sets the new status of the task",
	)
}
