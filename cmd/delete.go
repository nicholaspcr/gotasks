package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/nicholaspcr/gotasks/tasks"
)

// deleteCmd represents the close command
var deleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "deletes a task",
	Long: `delete the task that contains the id given,
in doesn't in fact delete it but moves it to a specific status 'bucket'`,
	Run: func(cmd *cobra.Command, args []string) {
		if taskID == -1 {
			fmt.Println("enter a valid taskID")
			os.Exit(1)
		}
		tasks.DBsetup()
		tasks.DeleteTask(taskID)
	},
}

func init() {
	rootCmd.AddCommand(deleteCmd)
}
