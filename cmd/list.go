package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/nicholaspcr/gotasks/tasks"
)

var filter, status string

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List all tasks",
	Long: `Able to lists all tasks,
it is possible to use filter or to select only one status.`,
	Run: func(cmd *cobra.Command, args []string) {
		if !tasks.IsValidFilter(filter) {
			fmt.Println("Enter a valid filter")
			os.Exit(1)
		}
		if !tasks.IsValidStatus(status) {
			fmt.Println("enter a valid status")
			os.Exit(1)
		}
		tasks.DBsetup()
		tasks.ListTasks(filter, status)
	},
}

func init() {
	rootCmd.AddCommand(listCmd)
	listCmd.Flags().StringVarP(&filter,
		"filter",
		"f",
		"",
		`Type of existant filters:
			{ Priority, Deadline, AddedTime } `,
	)
	listCmd.Flags().StringVarP(&status,
		"status",
		"s",
		"all",
		`Able to select which status to list.
		{ all, todo, working, closed, done }`,
	)
}
