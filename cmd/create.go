package cmd

import (
	"log"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/nicholaspcr/gotasks/tasks"
)

// local flags
var title string
var description string
var priority int
var deadline string
var estimate int

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:   "create",
	Short: `creates a new task`,
	Long:  `command to create a new task, the use of title is mandatory and the rest is optional`,
	Run: func(cmd *cobra.Command, args []string) {
		if title == "" {
			log.Fatal("Please give a title to the task")
		}
		if !tasks.IsValidPriority(priority) {
			log.Fatal("Please enter a valid priority")
		}
		layoutISO := "2006-01-02"
		deadlineDate, err := time.Parse(layoutISO, deadline)
		estimateTime := time.Duration(estimate) * time.Hour
		if err != nil {
			log.Fatal("Failed to parse deadline")
		}
		tasks.DBsetup()
		tasks.CreateTask(tasks.Task{
			Title:       title,
			Description: description,
			Priority:    priority,
			Deadline:    deadlineDate,
			Estimate:    estimateTime,
			Status:      tasks.Todo,
		})

	},
}

func init() {
	rootCmd.AddCommand(createCmd)
	createCmd.Flags().StringVarP(&title,
		"title",
		"t",
		"",
		"sets the title of task",
	)
	createCmd.Flags().StringVar(&description,
		"description",
		"",
		"description of the task",
	)
	createCmd.Flags().IntVarP(&priority,
		"priority",
		"p",
		0,
		`priority for the task. 
		{0->none, 1->low, 2->medium, 3->high}`,
	)
	createCmd.Flags().StringVarP(&deadline,
		"deadline",
		"d",
		"0001-01-01",
		"deadline of the task in iso format.",
	)
	createCmd.Flags().IntVarP(&estimate,
		"estimate",
		"e",
		0,
		"time estimated to finish the task, in hours",
	)
}
