# gotasks-cli

## installation

To install the cli-gotasks a few steps are necessary.

To make sure you have everything ready, please check the following list:  
  - Install the lastest go version available [here](https://golang.org/doc/install).
  - Enable modules by typing `GO111MODULE=on` on the terminal.
  - For last make sure that you added the go binaries to your path, a simple but temporary way to do this is `export PATH=$PATH:$GOPATH/bin`.<br>
  For a more permanent configuration there are a few options but one would be to add the same command to the `.profile` file in your system.

Now that everything is set up the process of installing the cli-gotasks is rather simple. 

After everything is ready to install the binary simply type

`go install gitlab.com/nicholaspcr/gotasks`

The reason for this is because `go install` builds the binary and moves it to the `go/bin/` folder in your system.

## Database

All tasks are stored as a sequence of bytes in the `$HOME/.gotasks/bolt.db` file, the executable makes sure the `.gotasks` folder is created if it doesn't already exist.


#### TODO
  - proper `--help` text in the cli command
  - Task blockage
    - currently very primitive
    - could find the item bigger than newTask using binary search ( in deadline )
  - Proper testing
    - need to figure how to tests the functions properly 
    - testing functions that use boltdb and cobra, what is the best approach.
  - Error handling
    - was very verbose
    - look what is necessary to check
    - is a error package a good thing to implement here?
    - think about error handling, errorCodes and messages.
  - Calendar
    - dynamic progrmaming? if so, how to model it?