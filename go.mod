module gitlab.com/nicholaspcr/gotasks

go 1.15

require (
	github.com/boltdb/bolt v1.3.1
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	golang.org/x/sys v0.0.0-20201211090839-8ad439b19e0f // indirect
)
